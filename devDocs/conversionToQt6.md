**General Things**
* Switch over to cmake
* Lower the resolution requirements down to 1280x720
* Should work with Plasma 6 + Wayland
* Virtualkeyboard should push properly instead of cover half the screen

**Settings**
* Switch over import `Qt.labs.settings 1.1` to `import QtCore`
* Replace `filename` with `location` with the "file://" as needed

**Material**
* CDAppWindow.qml:
    * Material.theme: Material.Light
    * Material.accent: "#62A8E5";
    * Material.primary: "#64aae5";
* For schedule: need to make it a rectangle with mouse area instead of button
* `if((rootWin.visibility == ApplicationWindow.Windowed) && (constsL.getNodeType() === "touch")) {`


**Graphical Effects**
* Replace `QtGraphicalEffects 1.12` with `Qt5Compat.GraphicalEffects`
* Fun with Shader baking (qsb / qsb6)

**Qt3D**
* CDToothScene3D.qml: DirectionalLight: intensity: .6
* Redo all the shaders to make it bakeable

**Multimedia**
* Switch the import to `import QtMultimedia`
* Have multiple `MediaPlayer`; one for audio and two for video
* No more playlist! Have to implement my own.

**Database**
* Move the actual DB to /var/clearDental/data/
* Put the touch and UI/UX settings to /etc/clearDental/nodeType.txt and ui.ini 