﻿Package name: xserver-xorg-input-evdev

Where to put: /etc/X11/xorg.conf

What to put in:

Section "InputClass"
        Identifier "evdev tablet catchall"
        MatchIsTablet "on"
        MatchDevicePath "/dev/input/event*"
        Driver "evdev"
EndSection

